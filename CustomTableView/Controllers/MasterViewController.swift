//
//  ViewController.swift
//  CustomTableView
//
//  Created by Mahesh Varadaraj on 14/1/19.
//  Copyright © 2017 Belal Khan. All rights reserved.
//

import UIKit
import Alamofire

var objLoc:customObject = customObject()

class MasterViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,HttpWrapperDelegate{



    var fetchingMore =  false
    let carList = HttpWrapper()


    @IBOutlet weak var tableList: UITableView!
    
    // MARK: - Table view data source


    
    override func viewDidLoad() {
        super.viewDidLoad()
     

        tableList.register(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: "cellLoading")
        tableList.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "cellList")
        tableList.tableFooterView = UIView()

        //fetching data from web api
        if !Connectivity.isConnectedToInternet() {
            alert(message: "No internet connection")
            tableList.isHidden = true
            return
        }else{
            tableList.isHidden = false

        }
        LoadingIndicatorView.show("")

        
        
    }
    //getCarListing
    func getListofCars(pageSize : String) {
        

        let strURL = APPURL.urlGetCars+"pageSize="+pageSize+"&wa_key="+APPURL.key
        carList.delegate = self
        carList.getListofCars(URL: strURL)
        
    }
    //MARK: - Delegates
    
    //fetchDataSuccess
    func HTTPWrapper(wrapper: HttpWrapper?, fetchDataSuccess dicsResponse: AnyObject) {
        let JSON = dicsResponse as! NSDictionary
       // ViewControllerUtils().hideActivityIndicator(uiView: self.view)
        LoadingIndicatorView.hide()


        objLoc.arrayItems = JSON.value(forKey: "wkda") as! NSDictionary
        self.fetchingMore = false
        self.tableList.reloadData()
        
    }
    
    //fetchDataFail
    func HTTPWrapper(wrapper: HttpWrapper?, fetchDataFail dicsResponse : AnyObject) {
        self.alert(message: "No Data Available")
    }

    
  
    
    

    // MARK: - Table view data source

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if section == 0 {
            return objLoc.arrayItems.count
        }else if section == 1 && fetchingMore {
            return 1
        }
        return 0
    }
    
    
    //the method returning each cell of the list
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellList", for: indexPath) as! ListTableViewCell
            
            
            if let valuesSting = objLoc.arrayItems.allValues as? [String] {
                var values = valuesSting
                values = values.sorted { $0 < $1 }
                cell.labelTitle.text = values[indexPath.row]
                
            }
            
            cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.red.withAlphaComponent(0.05) : .white
            cell.selectionStyle = .none

            return cell
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLoading", for: indexPath) as! LoadingTableViewCell
            cell.selectionStyle = .none

            cell.loadingSpinner.startAnimating()
            
            return cell
            
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

         var keysSting = objLoc.arrayItems.allKeys as? [String]
        var valuesString = objLoc.arrayItems.allValues as? [String]

            keysSting = keysSting?.sorted { $0 < $1 }
            valuesString = valuesString?.sorted { $0 < $1 }

        objLoc.manufStr = keysSting?[indexPath.row]
            objLoc.titleStr = valuesString?[indexPath.row]
        
        
        self.performSegue(withIdentifier: "segueDetails", sender: self)
    }

    
    /// add Pagianation

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height {
                if !self.fetchingMore{
                    self.beginFetch()
            
            }
            
        }
    }
    func beginFetch(){
        self.fetchingMore = true
        self.tableList.reloadSections(IndexSet(integer: 1), with: .none)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            if !Connectivity.isConnectedToInternet() {
                self.alert(message: "No internet connection")
                return
            }else{
                self.getListofCars(pageSize: String(objLoc.arrayItems.count + 15))
            }
        }
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }



}

