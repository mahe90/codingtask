//
//  File.swift
//  CodingTask
//
//  Created by Mahesh Varadaraj on 14/1/19.
//  Copyright © 2017 Belal Khan. All rights reserved.
//

import Foundation
import Alamofire


//MARK: - Delegates

@objc protocol HttpWrapperDelegate : NSObjectProtocol {
    
    @objc optional func HTTPWrapper(wrapper: HttpWrapper?, fetchDataSuccess dicsResponse: AnyObject)
    @objc optional func HTTPWrapper(wrapper: HttpWrapper?, fetchDataFail dicsResponse: AnyObject)
}

//MARK: - Happy Coding

class HttpWrapper : NSObject {
    
    //Declare Delegate
    weak var delegate:HttpWrapperDelegate?
    
    //RequestFunction
    func getListofCars(URL : String){
       
        Alamofire.request(URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (responseObject) -> Void in
            
            if responseObject.result.isSuccess {
                let jsonResponse = responseObject.result.value as! NSDictionary
                
                self.delegate?.HTTPWrapper!(wrapper: self, fetchDataSuccess: jsonResponse)
                
            }
            if responseObject.result.isFailure {
                let jsonResponse = responseObject.result.error
                self.delegate?.HTTPWrapper!(wrapper: self, fetchDataFail: jsonResponse as AnyObject)
            }
        }
    }
}
class Connectivity {
    class func isConnectedToInternet() -> Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
