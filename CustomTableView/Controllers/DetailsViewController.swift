//
//  DetailsViewController.swift
//  CustomTableView
//
//  Created by Mahesh Varadaraj on 14/1/19.
//  Copyright © 2019 Belal Khan. All rights reserved.
//

import UIKit
import Alamofire

class DetailsViewController: UIViewController,HttpWrapperDelegate,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableDetails: UITableView!
   
    let detilList = HttpWrapper()

    override func viewDidLoad() {
        super.viewDidLoad()

        tableDetails.register(UINib(nibName: "ListTableViewCell", bundle: nil), forCellReuseIdentifier: "cellList")
        tableDetails.estimatedRowHeight = 120.0
        tableDetails.tableFooterView = UIView()

        
        if !Connectivity.isConnectedToInternet() {
            alert(message: "No internet connection")
            tableDetails.isHidden = true
            return
        }else{
            LoadingIndicatorView.show("Loading...")
             tableDetails.isHidden = false
            getCarDetails()
        }
        

        self.title = objLoc.titleStr
        // Do any additional setup after loading the view.
        

        
    }
    //getCarDetails
    func getCarDetails() {
        
        
        
        let strURL = APPURL.urlGetDetails+objLoc.manufStr!+"&page=0&pageSize=040&wa_key="+APPURL.key
        detilList.delegate = self
        detilList.getListofCars(URL: strURL)
        
    }
    //MARK: - Delegates
    
    //fetchDataSuccess
    func HTTPWrapper(wrapper: HttpWrapper?, fetchDataSuccess dicsResponse: AnyObject) {
        let JSON = dicsResponse as! NSDictionary
        
        DispatchQueue.main.async {
            LoadingIndicatorView.hide()

        }


        objLoc.arrayDetails = JSON.value(forKey: "wkda") as! NSDictionary
        
        self.tableDetails.reloadData()
        
    }
    
    //fetchDataFail
    func HTTPWrapper(wrapper: HttpWrapper?, fetchDataFail dicsResponse : AnyObject) {
        self.alert(message: "No Data Available")
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
            return objLoc.arrayDetails.count

    }


     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellList", for: indexPath) as! ListTableViewCell
        
        
        if let valuesSting = objLoc.arrayDetails.allValues as? [String] {
            cell.labelTitle.text = valuesSting[indexPath.row]
        }
        cell.backgroundColor = indexPath.row % 2 == 0 ? UIColor.red.withAlphaComponent(0.05) : .white
        cell.selectionStyle = .none

            return cell




    }
  

     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

         let valuesSting = objLoc.arrayDetails.allValues as? [String]
        alert(message: String(format: "You have chosen : %@ Manufacturer :  %@",valuesSting![indexPath.row],objLoc.titleStr!))

    }

}
