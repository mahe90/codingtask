//
//  ListTableViewCell.swift
//  CodingTask
//
//  Created by Mahesh Varadaraj on 15/1/19.
//  Copyright © 2019 Mahesh Varadaraj. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {
    @IBOutlet weak var labelTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
